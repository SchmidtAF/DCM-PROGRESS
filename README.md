# Getting Started with DCM-PROGRESS

## the DCM-PROGRESS model
The DCM-PROGRESS score has been trained on a sample of dilated cardiomyopathy 
(DCM) patients, and predicts the 5-years risk of end-stage heart failure. 

The model takes six input features: NYHA class, R-axis, hear rate, TAPSE, systolic blood pressure,
and height. 
Please refer to the original publication describing the score in full [detail](https://www.medrxiv.org/content/10.1101/2023.09.10.23295251v1).

## Installing the python API

The current API can readily installed using git, conda, and pip. 

First clone the current repository

```bash
git clone git@gitlab.com:SchmidtAF/DCM-PROGRESS.git
cd DCM-PROGRESS
```

Next, to install the required dependencies please us `conda` to run 

```bash
# From the root
conda env create --file resources/conda/env/conda_create.yaml 
```

Finally, pip install DCM-PROGRESS

```bash
# From the root of DCM-PROGRESS
conda activate DCM-PROGRESS
python -m pip install .
```

Then start python and import the module to test

```python
# start python
python
# import module
import dcm_progress
```

### Confirm installation
After installation you may want to run the available pytests.
If you have cloned the repository you can run the command below from the 
root of the repository:

Run the tests using `pytest ./tests`

## Examples

Please consult the [tutorial](resources/examples/tutorial.ipynb) in `resources/examples/`.
You can run through this using jupyter:

```
conda activate DCM-PROGRESS
jupyter-notebook resources/examples/tutorial.ipynb
```

