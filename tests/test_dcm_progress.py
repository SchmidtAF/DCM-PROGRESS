'''
Pytests for dcm_progress. For now mostly focussing on the Calculate class,
might implement tests for the plotting functions if needed (i.e., depending on
usage).
'''

import pandas as pd
import numpy as np
from dcm_progress.predict import Calculate
from dcm_progress.example_data import examples


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# constants
INT = 'intercept'
RISK_CRUDE ='risk (crude)'
RISK_CORRECT ='risk (calibrated)'

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TestCalculate_calc_dcm_progress(object):
    '''
    Testing Calculate.calc_dcm_progress
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_null(self):
        '''
        Testing null input
        '''
        res = Calculate.calc_dcm_progress(nyha=0, height=0, heartrate=0, sbp=0,
                                    raxis=0, tapse=0)
        # confirming this is equal to the intercept
        expect = 1/(1+np.exp(-float(res[INT])))
        assert res[RISK_CRUDE] == expect
        assert isinstance(res, pd.Series)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_example_patinet(self):
        '''
        A none null example patient - already centered
        '''
        res = Calculate.calc_dcm_progress(nyha=1.0, height=-1.150062,
                                          heartrate=0.366542, sbp=2.237521,
                                          raxis=0.287811, tapse=-0.168321)
        assert np.round(res[RISK_CRUDE],4) == 0.0709
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TestCalculate_calculate_risk(object):
    '''
    Testing Calculate.calculate_risk
    '''
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_single_entry(self):
        '''
        Testing a single entry
        '''
        fit = Calculate(nyha=1, height=182, heartrate=90, sbp=70, raxis=-5,
                        tapse=25,
                       )
        res=fit.calculate_risk()
        assert isinstance(res.supplied_data, pd.DataFrame)
        assert isinstance(res.derived_data, pd.DataFrame)
        assert isinstance(res.relative_risk, pd.DataFrame)
        assert isinstance(res.relative_risk_absolute, pd.DataFrame)
        # eval output
        assert res.derived_data[RISK_CORRECT].round(4)[0] == 0.3050
        res2=fit.calculate_risk(corrected=False)
        assert res2.derived_data[RISK_CRUDE].round(4)[0] == 0.2135
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_single_entry_wo_standardisation(self):
        '''
        Testing a single entry without standarisation
        '''
        fit = Calculate(nyha=1, height=-20, heartrate=0, sbp=0, raxis=-5,
                        tapse=25, standardise=False,
                       )
        res=fit.calculate_risk(corrected=False)
        assert res.derived_data[RISK_CRUDE].round(4)[0] == 0.0022
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def test_data_frame_entry(self):
        '''
        Testing a data_frame entry
        '''
        data = examples.get_data('example_dataset')
        # explain how the columns should be mapped
        fit = Calculate(nyha='NYHA', height='height',
                                heartrate='heart-rate', sbp='sbp',
                                raxis='r_axis', tapse='tapse',
                                data=data,
                               )
        fit.calculate_risk()
        # assert
        assert fit.derived_data.mean(axis=0).to_list() == \
            [1.0584236993791838, 0.017599966436507027, -0.003390298942435241,
             -0.001001109054732097, 0.5613003945183344, 0.1219262157645,
             -2.37859717442555, 2.57177056004326, 2.60355946538468,
             0.6493933032732653
             ]
