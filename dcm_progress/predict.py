'''
This module will implement the DCM-PROGRESS model, predicting 5-years risk of
end-stage heart failure in patient with dilated cardiomyopathy.

Additionally, there is some code to calculate the patient characteristic
driving the predicted risk. Please `note` that irrespective of the contribution
to the predicted 5-years risk, the relative contribution of a characteristic
says little about the causal consequence of an intervention. In other words
this is a risk prediction module, not a causal model. Likely, in many cases
simply following guideline recommended treatment intensification is the better
strategy, rather than targeting a specific characteristic say heart rate.

Notes
-----
Please read, and where relevant, cite the original publication:
`Here <ADD URL>`_.
'''

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# imports
import copy as cp
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from typing import Any, List, Type, Union, Tuple, Optional, Dict
from dcm_progress.example_data import examples

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# constants
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Features(object):
    '''
    A feature names class
    '''
    nyha                   = 'NYHA class'
    height                 = 'Height'
    raxis                  = 'R-axis'
    heartrate              = 'Heart rate'
    sbp                    = 'SBP'
    tapse                  = 'TAPSE'
    intercept              = 'intercept'
    correction_intercept   = 'cor-intercept'
    correction_slope       = 'cor-slope'
    id                     = 'patient id'
    risk_crude             = 'risk (crude)'
    risk_correct           = 'risk (calibrated)'
    data_in                = 'supplied_data'
    data_out               = 'derived_data'
    relative_risk          = 'relative_risk'
    relative_risk_absolute = 'relative_risk_absolute'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# the weights, mean, and standard deviation
WEIGHT_DICT = {
    Features.nyha                 : 0.470410533057415,
    Features.height               : -0.0265394369385934,
    Features.raxis                : 0.0571035336275692,
    Features.heartrate             : 0.0519451495464189,
    Features.sbp                  : -0.339808283555482,
    Features.tapse                : -0.178213213180925,
    Features.intercept            : -2.37859717442555,
    Features.correction_intercept : 2.57177056004326,
    Features.correction_slope     : 2.60355946538468,
}
MEAN_DICT = {
    Features.height   : 175.62905982906,
    Features.raxis    : 9.45641025641026,
    Features.heartrate : 78.5871794871795,
    Features.sbp      : 120.41452991453,
    Features.tapse    : 18.734188034188,
}
SD_DICT = {
    Features.height   : 9.24216517190623,
    Features.raxis    : 54.0062569730762,
    Features.heartrate : 17.4954354900036,
    Features.sbp      : 19.926281940154,
    Features.tapse    : 4.36183256385094,
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class InputValidationError(Exception):
    pass

def _is_type(param: Any, types: Union[Tuple[Type], Type]) -> bool:
    """
    Checks if a given parameter matches any of the supplied types
    
    Parameters
    ----------
    param: object to test
    types: either a single type, or a tuple of types to test against.
    
    Returns
    -------
    True if the parameter is an instance of any of the given types.
    Raises InputValidationError otherwise.
    """
    if not isinstance(param, types):
        raise InputValidationError(f"Expected any of [{types}], got {type(param)}")
    return True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _are_columns_in_df(
    df: pd.DataFrame, expected_columns: Union[List[str], str],
    warning: bool=False
) -> bool:
    """
    Checks if all expected columns are present in a given pandas.DataFrame.
    
    Parameters
    ----------
    df: pandas.DataFrame
    expected_columns: either a single column name or a list of column names to test
    warning : bool, default False
        raises a warning instead of an error.
    
    
    Returns
    -------
    True if all expected_columns are in the df. Raises InputValidationError otherwise.
    """
    # constant
    message = "The following columns are missing from the pandas.DataFrame: {}"
    res = True
    # tests
    expected_columns_set: Set[str] = set(expected_columns) if isinstance(
        expected_columns, list
    ) else set([expected_columns])
    
    missing_columns = expected_columns_set - set(df.columns)
    # return
    if missing_columns:
        if warning == False:
            raise InputValidationError(
                message.format(missing_columns)
            )
        else:
            warnings.warn(
                message.format(missing_columns)
            )
            res = False
    return res

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Calculate(object):
    '''
    Calculating the 5-years risk of end-stage heart failure in people with
    dilated cardiomyopathy.
    
    References
    ----------
    `Here <TODO>`_.
    
    Parameters
    ----------
    nyha: string or integer
        The NYHA class supplied as 1,2,3, or 4.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    height: string or float,
        Height in centimeters, should be larger than 0.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    heartrate: string or float
        Heart rate in beats per minute, should be larger than 0.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    sbp: string or float,
        Sytolic blood pressure in mmHG, should be larger than 0.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    raxis: string or float,
        R-axis in degrees, can be negative or positive.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    tapse: string or float,
        Tapse in milimeters, should be larger than 0.
        Supply as string indicating the column in `data`. Alternatively, supply
        as a single value when calculating the risk for an individual patient.
    data : pandas.DataFrame, default `NoneType`
        A DataFrame describing clinical characteristic of multiple patients.
        This will be ignored when set to `NoneType`.
    
    Attributes
    ----------
    supplied_data: pd.DataFrame
        Contains the supplied data.
    
    Methods
    -------
    calculate_risk(corrected)
        Calculates the risk of end-stae HF for a `DataFrame` of one or more
        patients
    calc_dcm_progress(nyha, height, heartrate, sbp, raxis, tapse)
        Calculates the risk of end-stage HF based on supplied floats and
        integers. Helper function for `calculate_risk`.
    '''
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __init__(self,
                 nyha:Union[str, int],
                 height:Union[str, float],
                 heartrate:Union[str, float],
                 sbp:Union[str, float],
                 raxis:Union[str, float],
                 tapse:Union[str, float],
                 data:Union[pd.DataFrame, None]=None,
                 standardise:bool=True,
                 ):
        '''
        Assign data to self
        '''
        # #### constants
        self._EXP_COLUMNS = [Features.nyha, Features.height, Features.heartrate,
                            Features.sbp, Features.raxis, Features.tapse]
        # #### test input
        _is_type(data, (type(None), pd.DataFrame))
        _is_type(standardise, bool)
        # types
        if not isinstance(data, pd.DataFrame):
            # check type
            _is_type(nyha, (int, float))
            _is_type(height, (int,float))
            _is_type(heartrate, (int,float))
            _is_type(raxis, (int,float))
            _is_type(tapse, (int,float))
            # make dataframe
            data = pd.DataFrame({
                Features.nyha:  nyha,
                Features.height: height,
                Features.raxis: raxis,
                Features.heartrate: heartrate,
                Features.sbp: sbp,
                Features.tapse: tapse,
            }, index=[0])
            data.index.name = Features.id
            setattr(self,Features.data_in, data)
        else:
            # check all are strings
            _is_type(nyha, str)
            _is_type(height, str)
            _is_type(heartrate, str)
            _is_type(raxis, str)
            _is_type(tapse, str)
            # check if columns are present
            input_columns = [nyha, height, heartrate,sbp, raxis, tapse]
            _are_columns_in_df(data, input_columns)
            # subset data & rename ORDER dependent!
            setattr(self,Features.data_in, data[input_columns].copy())
            self.__dict__[Features.data_in].columns = self._EXP_COLUMNS
        # do we need to standardise
        if standardise == True:
            if MEAN_DICT.keys() != SD_DICT.keys():
                raise KeyError('`MEAN_DICT` and `SD_DICT` do not have the same '
                               'keys. The code-base has been corrupted, please '
                               'do not use this implementation and contact the '
                               'authors.'
                               )
            for col in MEAN_DICT.keys():
                self.__dict__[Features.data_in][col] =\
                    (self.__dict__[Features.data_in][col] - MEAN_DICT[col])/\
                    SD_DICT[col]
                # data[col] = (data[col] - MEAN_DICT[col])/SD_DICT[col]
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def __str__(self):
        return f"DCM-PROGRESS calculator"
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @staticmethod
    def calc_dcm_progress(
        nyha:int,
        height:float,
        heartrate:float,
        sbp:float,
        raxis:float,
        tapse:float,
    ) -> pd.Series:
        '''
        Function to calculate the DCM-PROGRESS score for a single individual
        based on integer and float inputs.
        
        Parameters
        ----------
        nyha: integer
            The NYHA class supplied as 1,2,3, or 4.
        height: float,
            Height in centimeters, should be larger than 0.
        heartrate: float
            Heart rate in beats per minute, should be larger than 0.
        sbp: float,
            Sytolic blood pressure in mmHG, should be larger than 0.
        raxis: float,
            R-axis in degrees, can be negative or positive.
        tapse: float,
            Tapse in milimeters, should be larger than 0.
        
        Returns
        -------
        pd.Series,
            Return a pandas DataFrame with the intercept, recalibration
            intercept and slope, the individual inputs multiplied by the
            weights, and the 5-years risk of end-stage heart failure mapped to
            a zero and one interval (uncorrected and corrected for the
            recalibration intercept and slope).
        '''
        # #### test input
        _is_type(nyha, (int, float))
        _is_type(height, (int, float))
        _is_type(heartrate, (int, float))
        _is_type(sbp, (int, float))
        _is_type(raxis, (int, float))
        _is_type(tapse, (int, float))
        # ### create Series
        series = pd.Series({
            Features.nyha                 : WEIGHT_DICT[Features.nyha] * nyha,
            Features.height               : WEIGHT_DICT[Features.height] * height,
            Features.raxis                : WEIGHT_DICT[Features.raxis] * raxis,
            Features.heartrate            : WEIGHT_DICT[Features.heartrate] * heartrate,
            Features.sbp                  : WEIGHT_DICT[Features.sbp] * sbp,
            Features.tapse                : WEIGHT_DICT[Features.tapse] * tapse,
            Features.intercept            : WEIGHT_DICT[Features.intercept],
            Features.correction_intercept : WEIGHT_DICT[Features.correction_intercept],
            Features.correction_slope     : WEIGHT_DICT[Features.correction_slope],
        })
        # sum and calculate the risk
        sum_crude = np.sum(series[:-2])
        sum_correct = sum_crude * series[Features.correction_slope] +\
            series[Features.correction_intercept]
        # inverse logit
        risk_crude = 1/(1+np.exp(-sum_crude))
        risk_correct = 1/(1+np.exp(-sum_correct))
        # add to series
        series[Features.risk_crude] = risk_crude
        series[Features.risk_correct] = risk_correct
        # ### return
        return series
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    def calculate_risk(self,
                       corrected:bool=True,
                       copy:bool=True,
                       ):
        '''
        Will calculate the 5-years risk of end-stage HF for people with
        dilated cardiomyopathy. The instance method is intended two work on
        an (internally generated) pd.DataFrame, and will return a DCM-PROGRESS
        results class.
        
        Parameters
        ----------
        corrected: bool, default `True`
            Whether the predicted risk should be based on the re-calibrated
            model or not.
        copy: bool, default `True`
            Whether to return a shallow copy of self or not.
        
        Attributes
        ----------
        supplied_data: pd.DataFrame
            Contains the supplied data.
        derived_data: pd.DataFrame
            Contains the returned data, listing the intercept, calibration
            correction factors, the input data multiplied by the model weights and
            the estimated risk of end-stage HF.
        relative_risk: pd.DataFrame
            Contains the relative contribution of each each weighted clinical
            characteristics. This is simply defined as each row value  in `data_out`
            divided by the row sum.
        relative_risk_absolute: pd.DataFrame
            Same as `relative_risk`, but applying an absolute transformation before
            performing any calculation.
        
        Returns
        -------
        self
            Returns a DCM-PROGRESS `ResultsObject` class packaging the supplied
            data, the estimated risk, and the relative contributions to the
            risk.
        '''
        # #### check input
        _is_type(corrected, bool)
        # #### calculate risk
        data_out_dict = {}
        for _, x in self.__dict__[Features.data_in].iterrows():
            s_r = self.calc_dcm_progress(
                 nyha=int(x[Features.nyha]),
                 height=float(x[Features.height]),
                 heartrate=float(x[Features.heartrate]),
                 sbp=float(x[Features.sbp]),
                 raxis=float(x[Features.raxis]),
                 tapse=float(x[Features.tapse]),
            )
            # combining
            data_out_dict[x.name] = s_r
        # turning into DataFrame
        setattr(self,Features.data_out,
                pd.DataFrame.from_dict(data_out_dict, orient='index')
                )
        # calculate relative risk
        temp_df = self.__dict__[Features.data_out][self._EXP_COLUMNS]
        setattr(self,Features.relative_risk,
                (temp_df.T/temp_df.sum(axis=1)).T
                )
        # without sign
        temp_df_abs = np.abs(temp_df)
        setattr(self,Features.relative_risk_absolute,
                (temp_df_abs.T/temp_df_abs.sum(axis=1)).T
                )
        # ### which risk to return
        if corrected == True:
            del self.__dict__[Features.data_out][Features.risk_crude]
        else:
            del self.__dict__[Features.data_out][Features.risk_correct]
        # ### returning
        if copy == True:
            return cp.copy(self)
        else:
            return self

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# figures
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _update_kwargs(update_dict:Dict[Any, Any], **kwargs:Optional[Any],
            ) -> Dict[Any, Any]:
    '''
    This function will take any number of `kwargs` and add them to an
    `update_dict`. If there are any duplicate values in the `kwargs` and the
    `update_dict`, the entries in the `update_dict` will take precedence.
    
    Parameters
    ----------
    update_dict : dict,
        A dictionary with key - value pairs that should be combined with any
        of the supplied kwargs.
    kwargs :
        Arbitrary keyword arguments.
    
    Returns
    -------
    dict
        A dictionary with the update_dict and kwargs combined, where duplicate
        entries from update_dict overwrite those in kwargs.
    
    Examples
    --------
        The function is particularly useful to overwrite `kwargs` that are
        supplied to a nested function say
        
        >>> _update_kwargs(update_dict={'c': 'black'}, c='red',
                         alpha = 0.5)
        >>> {'c': 'black', 'alpha': 0.5}
    '''
    new_dict = {**kwargs, **update_dict}
    # returns
    return new_dict

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def bar(df:pd.DataFrame, label:str, column:str, ax:plt.Axes,
        colours:Union[str,List[str]]=['coral'], transparancy:float=0.7,
        wd:float=1, edgecolor:str='black',
        ascend:bool=False, percentage:bool=True,
        **kwargs:Optional[Any],
        ) -> plt.Axes:
    '''
    Plots a barchart with sequentially coloured bars.
    
    Arguments
    ---------
    df : pd.DF
    label : str
        The column name with the axes labels you want to use.
    column : str
        The column name with the (y-axis) values (floats/int) that need to be
        plotted.
    colours : string or list of strings,
        A list of colours, can be a single or multiple values (will get
        recycled).
    transparancy : float,
        For the alpha of the bars.
    wd : float,
        A float to specify bar widths.
    edgecolor : str
        The bar edgecolor.
    ax : plt.Axes
    ascend: bool, default `False`
        Whether the column values should be sorted in ascending order (`True`),
        descending order (`False`) or not `NoneTyep`.
    percentage: bool, default `True`
        Whether the column values should be multiplied by 100.
    kwargs
        Arbitrary keyword arguments for `ax.bar`.
    
    Returns
    -------
    plt.Axes
    '''
    # ### check input
    _is_type(column, str)
    _is_type(label, str)
    _is_type(colours, (str, list))
    _is_type(ax, plt.Axes)
    _is_type(wd, (float, int))
    _is_type(transparancy, (float, int))
    _is_type(ascend, (bool, type(None)))
    _is_type(percentage, bool)
    if any(df.isna().any()):
        raise ValueError('`df` contains missing values.')
    # #### plotting
    # copy so the original is not affected
    df = df.copy()
    # sort
    if isinstance(ascend, bool) == True:
        df.sort_values(column, ascending=ascend, inplace=True)
    # percentage
    if percentage == True:
        df[column] = df[column] * 100
    # get labels
    labels = df[label]
    # updating kwargs
    new_kwargs = _update_kwargs(update_dict=kwargs, edgecolor=edgecolor,
                                width=wd, color=colours,
                                alpha=transparancy,
                                )
    # actual plotting
    ax.bar(labels,height=df[column], **new_kwargs,
           )
    # removing spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    # default axes labels
    ax.set_ylabel('Relative contribution')
    ax.set_xlabel('')
    # return
    return ax


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def plot_reference(
    predicted_risk:float, ax:plt.Axes, density:bool=True, bins:int=30, lw=0.8,
    colour:str='darkorange', edgecolour='black', vcolour:str='black',
    vlw:float=0.70, vline:str='--', annot_loc:Tuple[float, float]=(0.75, 0.95),
    annot_font:float=8,
    **kwargs:Optional[Any],
) -> plt.Axes:
    '''
    A histogram comparing an individual's risk to a simulated version of the
    reference data, based on a simulated version of the predicted risk in the
    exteranl validation data.
    
    Arguments
    ---------
    predicted_risk: float,
        An individual's predicted risk.
    density: bool, default `True`
        Whether to plot the density or the frequency.
    bins: int, defeault 30
        The number of histogram bins.
    colour : str, default `darkorange`
        The fill colour for the histogram bars.
    lw : float, default 0.8
        A float to specify bar widths.
    edgecolor : str, default `black`
        The bar edgecolor.
    vcolour: str, default `black`
        The colour of the vertical reference line indicating the predicted risk.
    vlw: float, default 0.70
        The linewidth of the refernce line.
    vline: str, default `--`
        The linestyle of the refernece line.
    annot_loc: tuple of two floats, default `(0.75, 0.95)`
        The location of the annotations. Set to `NoneType` to skip annotations.
    annot_font: float, default 8
        The fontsize of the annotations.
    ax : plt.Axes
    kwargs
        Arbitrary keyword arguments for `ax.bar`.
    
    Returns
    -------
    plt.Axes
    '''
    # ### test intput
    _is_type(predicted_risk, float)
    _is_type(density, bool)
    _is_type(bins, (int, float))
    _is_type(colour, str)
    _is_type(edgecolour, str)
    _is_type(lw, (int, float))
    _is_type(ax, plt.Axes)
    _is_type(annot_loc, tuple)
    # ### load reference data
    reference = examples.get_data('load_reference_dataset').iloc[:,0].\
        to_numpy()
    # ### updating kwargs
    new_kwargs = _update_kwargs(update_dict=kwargs, edgecolor=edgecolour,
                                range=(0,1), density=density, bins=bins,
                                linewidth=lw, color=colour,
                                )
    # ### histogram
    ax.hist(reference, **new_kwargs)
    ax.set_ylabel('Density')
    ax.set_xlabel('Predicted risk')
    # removing spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    # ### add refernce line
    ax.axvline(predicted_risk, c=vcolour, linewidth=vlw, linestyle=vline,
               zorder=2)
    # ### annotate
    if not annot_loc is None:
        percentile = np.mean(reference <= predicted_risk)*100
        RISK = 'Predicted risk: {0:.2f}'.format(predicted_risk)
        PERCENTILE = 'Population percentile: {0:.2f}'.format(percentile)
        recal_textstr = '\n'.join((RISK, PERCENTILE))
        PROPS = dict(boxstyle='round', facecolor='white', alpha=0.7,
                     edgecolor='black', lw=0.4)
        ax.text(*annot_loc, recal_textstr , transform=ax.transAxes,
                fontsize=annot_font,
                verticalalignment='top', horizontalalignment='left',
                bbox=PROPS,
                )
    # return
    return ax

